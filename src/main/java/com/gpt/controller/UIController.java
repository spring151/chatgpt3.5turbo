package com.gpt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import com.gpt.dto.Message;

@Controller
@RequestMapping("/chatgpt")
public class UIController {

	@GetMapping("/chat")
	public String chat() {
		return "chat";
	}
	
	
	
	@Autowired
	RestTemplate restTemplate;

	public String search(Message message) {
		return restTemplate.postForEntity("http://localhost:8080/chatgpt/search", message, String.class).getBody();
	}
	
	@PostMapping("/chat")
	public String chat(@ModelAttribute Message message,Model model) {
		String replyText=search(message);
		System.out.println(replyText);
		model.addAttribute("message", replyText);
		return "chat";
	}
}
